package com.company;


import java.time.LocalDateTime;

public class Order {
    private ShoppingCart cart;
    private Credentials persona;
    STATUS status = STATUS.WAIT;
    private LocalDateTime TimeCompletedOrder = LocalDateTime.now();
    private LocalDateTime TimeDeleteOrder = TimeCompletedOrder.plusMinutes(5);

    public LocalDateTime getTimeCompletedOrder() {
        return TimeCompletedOrder;
    }


    public LocalDateTime getTimeDeleteOrder() {
        return TimeDeleteOrder;
    }


    public void setCart(ShoppingCart cart) {
        this.cart = cart;
    }

    public ShoppingCart getCart() {
        return cart;
    }


    public Credentials getPersona() {
        return persona;
    }

    public void setPersona(Credentials persona) {
        this.persona = persona;
    }


    public STATUS GetStatus() {
        return status;
    }

    public STATUS SetStatus() {
        status = STATUS.OK;
        return status;
    }


}
