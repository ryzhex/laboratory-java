package com.company;

import java.util.Scanner;

public class Coffee extends Product {
    String CoffeeSorts[] = {"arabica", "robusta"};
    String CoffeeSort;

    public Coffee() {

    }


    @Override
    public void create() {
        super.create();

        index = (int) (Math.random() * (CoffeeSorts.length));
        CoffeeSort = CoffeeSorts[index];

    }

    @Override
    public void read() {
        super.read();
        System.out.println(CoffeeSort);
    }

    @Override
    public void delete() {
        super.delete();
        CoffeeSort = "";

    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Сорт кофе=");
        CoffeeSort = sc.nextLine();
        sc.close();
    }

}