package com.company;


import java.util.Scanner;

public class Tee extends Product {
    String ColorsTee[] = {"green", "white", "black"};
    String ColorTee;

    public Tee() {

    }



    @Override
    public void create() {
        super.create();

        index = (int) (Math.random() * (ColorsTee.length));
        ColorTee = ColorsTee[index];

    }

    @Override
    public void read() {
        super.read();
        System.out.println(ColorTee);
    }

    @Override
    public void delete() {
        super.delete();

        ColorTee = "";

    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println( "Цвет чая=" );
        ColorTee = sc.nextLine();
        sc.close();
    }

}
