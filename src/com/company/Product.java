package com.company;


import java.util.Scanner;
import java.util.UUID;

public abstract class Product implements ICrudAction {
    static public int Count;
    private UUID id;
    private String Name;
    private int Prise;
    private String NameCountry;
    private String NameFirm;

    int index;
    String Names[] = {"name1", "name2", "name3", "name4", "name5"};
    int Prises[] = {100, 200, 4000, 7333, 439432};
    String NameCountries[] = {"Russia", "USA", "Germany"};
    String NameFirms[] = {"firm1", "firm2", "firm3", "firm4", "firm5"};


    public Product() {

    }

    @Override
    public void create() {

        id = UUID.randomUUID();

        Count++;
        index = (int) (Math.random() * (Names.length));
        Name = Names[index];

        index = (int) (Math.random() * (Prises.length));
        Prise = Prises[index];

        index = (int) (Math.random() * (NameCountries.length));
        NameCountry = NameCountries[index];

        index = (int) (Math.random() * (NameFirms.length));
        NameFirm = NameFirms[index];

    }


    @Override
    public void read() {
        System.out.println(Name);
        System.out.println(Prise);
        System.out.println(NameCountry);
        System.out.println(NameFirm);
    }

    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Страна производитель=");
        NameCountry = sc.nextLine();
        System.out.println("Цена=");
        Prise = sc.nextInt();
        sc.close();
    }

    @Override
    public void delete() {
        Name = "";
        Prise = 0;
        NameFirm = "";
        NameCountry = "";
        Count--;

    }
}
